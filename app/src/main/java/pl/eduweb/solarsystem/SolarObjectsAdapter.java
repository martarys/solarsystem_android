package pl.eduweb.solarsystem;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SolarObjectsAdapter extends RecyclerView.Adapter<SolarObjectsAdapter.SolarObjectViewHolder> {

    private final SolarObject[] solarObjects;
    private SolarObjectClickedListener solarObjectClickedListener;

    public SolarObjectsAdapter(SolarObject[] solarObjects) {

        this.solarObjects = solarObjects;
    }

    public void setSolarObjectClickedListener(SolarObjectClickedListener solarObjectClickedListener) {
        this.solarObjectClickedListener = solarObjectClickedListener;
    }

    @NonNull
    @Override
    public SolarObjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_solar_object, parent, false);
        return new SolarObjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SolarObjectViewHolder holder, int position) {
        SolarObject solarObject = solarObjects[position];
        holder.setSolarObject(solarObject);
    }

    @Override
    public int getItemCount() {
        return solarObjects.length;
    }

    private void itemClicked(SolarObject solarObject) {
        if(solarObjectClickedListener != null){
            solarObjectClickedListener.solarObjectClicked(solarObject);
        }
    }

    public void setSolarObjectClickedListener(SolarObjectsFragment solarObjectsFragment) {
        this.solarObjectClickedListener = solarObjectClickedListener;
    }

    class SolarObjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.itemImageView)
        ImageView itemImageView;
        @BindView(R.id.itemTextView)
        TextView itemTextView;

        private SolarObject solarObject;

        public SolarObjectViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setSolarObject(SolarObject solarObject) {
            this.solarObject = solarObject;

            itemTextView.setText(solarObject.getName());
            //biblioteka Glide - do wyświetlania grafiki, ładowanie asynchroniczne
            Glide.with(itemImageView.getContext())
                    .load("file:///android_asset/" + solarObject.getImage())
                    .placeholder(R.drawable.planet_placeholder)
                    .fitCenter()
                    .into(itemImageView);
        }

        public SolarObject getSolarObject() {
            return solarObject;
        }

        @Override
        public void onClick(View v) {
            itemClicked(solarObject);

        }
    }

    public interface SolarObjectClickedListener {
        void solarObjectClicked(SolarObject solarObject);
    }

}

///pomieszałam coś - implementacje nie w tych plikach co trzeba - pomyliłam Adaptera z fragmentem
