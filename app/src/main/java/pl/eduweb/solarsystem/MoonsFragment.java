package pl.eduweb.solarsystem;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MoonsFragment extends Fragment {

    public static final String OBJECTS_KEY = "objects";

    @BindView(R.id.moonsViewPager)
    ViewPager moonsViewPager;
//    @BindView(R.id.moonsTabLayout)
//    TabLayout moonsTabLayout;

    private TabCallback tabCallback;

    public MoonsFragment() {
        // Required empty public constructor
    }

    public static MoonsFragment newInstance(SolarObject[] solarObjects) {

        MoonsFragment fragment = new MoonsFragment();

        Bundle args = new Bundle();
        args.putSerializable(OBJECTS_KEY, solarObjects);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        tabCallback = (TabCallback) context;
    }
//odpinanie fragmentu od Activity - zerujemy callback:
    @Override
    public void onDetach() {
        super.onDetach();
        tabCallback = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_moons, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SolarObject[] solarObjects = (SolarObject[]) getArguments().getSerializable(OBJECTS_KEY);
        MoonsPagerAdapter moonsPagerAdapter = new MoonsPagerAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, solarObjects);
        moonsViewPager.setAdapter(moonsPagerAdapter);

        tabCallback.showTabs(moonsViewPager);
    }

    @Override
    public void onDestroyView(){
        tabCallback.hideTabs();
        super.onDestroyView();
    }

    public interface TabCallback {
        void showTabs(ViewPager viewPager);
        void hideTabs();
    }

}